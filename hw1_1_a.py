import matplotlib.pyplot as plt
import pandas as pd

dt_1 = pd.read_csv("spice.din", sep=" ", header=None, names=['opera', 'adr'])
dt_2 = pd.read_csv("tex.din", sep=" ", header=None, names=['opera', 'adr'])
df_1 = pd.DataFrame({'opera': dt_1.opera, 'adr': dt_1['adr'].apply(lambda x: int(x, 16))})
df_2 = pd.DataFrame({'opera': dt_2.opera, 'adr': dt_2['adr'].apply(lambda x: int(x, 16))})

plt.hist(x=df_2['adr'])
plt.xlabel('adr')
plt.ylabel('occur')
plt.title('histogram')
plt.show()
