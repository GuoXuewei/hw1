import numpy as np
import time

# The int of matrix running time
int_start_time = time.time()
for i in range(10):
    matrix_int_1 = np.random.randint(1, 10, (1000, 2000))
    matrix_int_2 = np.random.randint(1, 10, (2000, 1000))
    c = np.dot(matrix_int_1, matrix_int_2)
print('Total time spent of int: ', (time.time() - int_start_time)/10)
