import pandas as pd

df_1 = pd.read_csv("spice.din", sep=" ", header=None, names=['opera', 'adr'])
df1_count = pd.DataFrame(df_1['opera'].value_counts())
print(df1_count)
print('frequency of Spice reading: {:.2%}'.format(df1_count['opera'][0]/df1_count['opera'].sum()))
print('frequency of Spice writing: {:.2%}'.format(df1_count['opera'][1]/df1_count['opera'].sum()))
print('frequency of Spice fetching: {:.2%}'.format(df1_count['opera'][2]/df1_count['opera'].sum()))

df_2 = pd.read_csv("tex.din", sep=" ", header=None, names=['opera', 'adr'])
df2_count = pd.DataFrame(df_2['opera'].value_counts())
print(df2_count)
print('frequency of Tex reading: {:.2%}'.format(df2_count['opera'][0]/df2_count['opera'].sum()))
print('frequency of Tex writing: {:.2%}'.format(df2_count['opera'][1]/df2_count['opera'].sum()))
print('frequency of Tex fetching: {:.2%}'.format(df2_count['opera'][2]/df2_count['opera'].sum()))
