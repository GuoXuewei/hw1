import numpy as np
import time
from decimal import Decimal

double_start_time = time.time()
for i in range(10):
    matrix_double_1 = np.random.random(size=(1000, 2000))
    matrix_double_2 = np.random.random(size=(2000, 1000))
    c = np.matmul(matrix_double_1, matrix_double_2)
print('Total time spent of double: ', (time.time() - double_start_time)/10)
# print(c)
